
from django.db import models
from django.contrib.auth.models import AbstractUser
class CustomUser(AbstractUser):
    manager_user = models.BooleanField(default = False)
    phone_number = models.CharField(max_length = 15)
    

   