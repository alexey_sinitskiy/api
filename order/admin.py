from django.contrib import admin
from .models import Order


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'status', 'material', 'dateCreated', 'dateShipped', 'comment')


admin.site.register(Order, OrderAdmin)