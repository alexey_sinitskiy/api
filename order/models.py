from django.db import models
from django.contrib.auth import get_user_model
from django.conf import settings

#User = get_user_model()
User = settings.AUTH_USER_MODEL

class Order(models.Model):
    status_types = (
        (1, 'Новий'),
        (2, 'У роботі'),
        (3, 'Виконаний'),
        (4, 'Відхилено')
        )
    material_types = (
        (1, 'Деревина'),
        (2, 'Кераміка'),
        (3, 'Камінь'),
        (4, 'Дсп'),
        (5, 'Сталь'),
        (6, 'Скло'),
        )

    status = models.IntegerField(verbose_name = 'Status_types', choices = status_types)
    material = models.IntegerField(verbose_name = 'Material_types', choices = material_types)
    user = models.ForeignKey(User, verbose_name = 'User', on_delete=models.CASCADE)
    dateCreated = models.DateField()
    dateShipped = models.DateField(blank=True, null=True)
    comment = models.CharField(verbose_name = 'Comment', max_length = 100, blank=True)
    phone_number = models.CharField(verbose_name = 'Phone number', max_length = 15, blank=True)


