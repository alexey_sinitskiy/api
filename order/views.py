from django.shortcuts import render
from .models import Order
from rest_framework import generics
from .serializers import OrderSerializer, UserSerializer
from .permission import IsOwner, IsManager
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from django.conf import settings
from rest_framework.viewsets import ModelViewSet
from django.contrib.auth import get_user_model

User = get_user_model()

#User = settings.AUTH_USER_MODEL


class OrderList(generics.ListAPIView):
    serializer_class = OrderSerializer
    permission_classes = ( IsOwner,)

    def get_queryset(self):
        user = self.request.user
        return Order.objects.filter(user=user)


class OrderCreateView(generics.CreateAPIView):
    serializer_class =  OrderSerializer
    permission_classes = ( IsAuthenticated, )


class OrderUpdateView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    permission_classes = (IsAdminUser, )


class UserCreate(generics.CreateAPIView):
    
    serializer_class = UserSerializer
    permission_classes = (AllowAny, )


class UserLogin(ModelViewSet):
    
    serializer_class = UserSerializer
    #queryset = User.objects.all()
    def get_queryset(self):
        userpass = self.request.user.password
        usern = self.request.user.username
        return User.objects.filter(password=userpass, username=usern)
