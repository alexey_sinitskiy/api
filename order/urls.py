from django.urls import path
from .views import OrderList, OrderCreateView, OrderUpdateView, UserCreate, UserLogin

urlpatterns = [
    path('order/', OrderList.as_view()),
    path('ordercreate/', OrderCreateView.as_view()),
    path('account/register/', UserCreate.as_view()),
    path('account/login/', UserLogin.as_view({'get': 'list'})),
    path('orderupdate/<int:pk>/', OrderUpdateView.as_view()),
]