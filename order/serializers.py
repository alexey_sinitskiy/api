from .models import Order
from django.conf import settings
from rest_framework import serializers
from django.contrib.auth import get_user_model


User = settings.AUTH_USER_MODEL

class OrderSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    class Meta:
        model = Order
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
       model = get_user_model()
       fields = ('id', 'password',  'username', 'is_staff', 'manager_user', 'phone_number')
       #fields = '__all__'

    def create(self, validated_data):
        password = validated_data.pop('password', '')
        user = self.Meta.model(**validated_data)
        user.set_password(password)
        user.save()
        return user